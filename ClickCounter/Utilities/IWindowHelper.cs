﻿namespace ClickCounter.Utilities
{
	public interface IWindowHelper
	{
		string GetActiveWindowTitle();

		string GetActiveProcessFileName();
	}
}
