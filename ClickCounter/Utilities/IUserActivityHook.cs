﻿using System;
using System.Windows.Forms;

namespace ClickCounter.Utilities
{
	public interface IUserActivityHook
	{
		event MouseEventHandler OnMouseActivity;
	}
}
