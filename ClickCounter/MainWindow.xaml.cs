﻿using ClickCounter.Utilities;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows;

namespace ClickCounter
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private readonly IWindowHelper windowHelper;
		private DirectoryInfo logDirectory;
		private StreamWriter streamWriter;
		private bool isStarted = false;

		public MainWindow(IWindowHelper helper, IUserActivityHook userActivityHook)
		{
			this.windowHelper = helper;
			InitializeComponent();
			InitializeLogDirectory();
			this.openLogsButton.Click += OpenLogsButton_Click;
			this.startButton.Click += StartButton_Click;
			this.Closed += MainWindow_Closed;
			userActivityHook.OnMouseActivity += UserActivityHook_OnMouseActivity;
		}

		private void UserActivityHook_OnMouseActivity(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (!this.isStarted || this.streamWriter == null || e.Clicks <= 0)
			{
				return;
			}
			this.streamWriter.WriteLine($"{DateTime.Now:G}:{this.windowHelper.GetActiveProcessFileName()}_{this.windowHelper.GetActiveWindowTitle()}");
		}

		private void InitializeLogDirectory()
		{
			string basePath = System.AppDomain.CurrentDomain.BaseDirectory;
			string logPath = System.IO.Path.Combine(basePath, "Logs");
			this.logDirectory = Directory.CreateDirectory(logPath);
		}

		private void MainWindow_Closed(object sender, EventArgs e)
		{
			this.streamWriter.Flush();
			this.streamWriter.Close();
		}

		private void StartButton_Click(object sender, RoutedEventArgs e)
		{
			this.isStarted = !this.isStarted;
			if (this.isStarted)
			{
				string logFilePath = System.IO.Path.Combine(this.logDirectory.FullName, $"log_{DateTime.Now:ddMMyyyyhhmmss}.txt");
				FileStream fileStream = File.Create(logFilePath);
				this.streamWriter = new StreamWriter(fileStream);
			}
			else
			{
				this.streamWriter.Flush();
				this.streamWriter.Close();
			}
		}

		private void OpenLogsButton_Click(object sender, RoutedEventArgs e)
		{
			Process.Start("explorer.exe", this.logDirectory.FullName);
		}
	}
}
