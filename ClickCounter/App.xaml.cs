﻿using ClickCounter.Utilities;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Windows;

namespace ClickCounter
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		private ServiceProvider serviceProvider;

		public App()
		{
			ServiceCollection services = new();
			ConfigureServices(services);
			serviceProvider = services.BuildServiceProvider();
		}

		private static void ConfigureServices(ServiceCollection services)
		{
			services.AddSingleton<IWindowHelper, WindowHelper>();
			services.AddSingleton<IUserActivityHook, UserActivityHook>();
			services.AddScoped<MainWindow>();
		}

		protected override void OnStartup(StartupEventArgs e)
		{
			MainWindow mainWindow = serviceProvider.GetService<MainWindow>();
			mainWindow.Show();
		}
	}
}
